
# MYSQL的原理

## Mysql的内存和磁盘结构

<img src="img/1-1.png" alt="img" style="zoom:75%;" />

<img src="img/1-save-to-disk.png" alt="img" style="zoom:75%;" />

<img src="img/1-3-save-4.png" alt="img" style="zoom:75%;" />

<img src="img/1-3-save.png" alt="img" style="zoom:75%;" />

## 事务原子性、持久性和一致性原理分析


## InnoDB的锁
    InnoDB的锁可细分为记录锁（Record Lock）、间隙锁（Gap Lock)、临键锁（Next_Key Lock），是基于索引实现的，其本质上是三种加锁算法。ps：若不声明，默认采用RR隔离级别。
    对记录加锁，其实是针对索引进行加锁的。
    根据哪个索引进行查寻，就是对哪个索引进行加锁。
    如果执行：update XXX  where name="中文"；
    主键锁引和次要索引都会加锁

    1、MYSQL的记录锁
    记录锁很好理解，一般要通过主键或唯一索引加锁，就可以较好的实现。

    2、MYSQL的临键锁
    在默认情况下，mysql的事务隔离级别是RR，并且innodb_locks_unsafe_for_binlog=0，这时默认采用next-key locks.所谓Next-Key Lock，就是Record lock和gap lock的结合。

    3、MYSQL的间隙锁
    间隙锁是一个在索引记录之间的间隙上的锁。
    作用：保证某个间隙内的数据在锁定情况下不会发生任何变化。比如mysql默认隔离级别下的可重复读（RR）。
    当使用唯一索引来搜索唯一行的语句时，不需要间隙锁定。如果语句的id列有唯一索引，此时只会对主键值为10的使用记录锁。
    需要注意的是，当id列上没有索引时，SQL会走聚簇索引的全表扫描进行过滤，由于过滤是在MySQL Server层面进行的。因此每条记录（无论是否满足条件）都会被加上X锁。但是，为了效率考量，MySQL做了优化，对于不满足条件的记录，会在判断后放锁，最终持有的，是满足条件的记录上的锁。但是不满足条件的记录上的加锁/放锁动作是不会省略的。所以在没有索引时，不满足条件的数据行会有加锁又放锁的耗时过程。





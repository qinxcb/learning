### 一、课程内容
1.全面理解员工的个人目标
    1.前三年：锻炼能力
    2.
2.马斯洛需求理论
    1.生理需求：水，食物，睡眼
    2.安全需求：人身安全，健康保障，工作保障：底薪 
    3.社交需求：友情，爱情，两性亲密关系
    4.尊重需求：自尊心，自信心，成就他人，被尊重：价值感和成就感
    5.自我实现需求：道德，创造力，公正度，接受现实能力
3.目标（短期：一年以内； 中期：）
    1.家庭生活： 短中长（20万，150平以上房子，资产5000万）
    2.本职工作：短中长（销售业绩前三名，带50人团队，年度创收10亿）
    3.个人发展：短中长（加入头部公司，晋升总监，晋升合伙人）
4. 不同人的层次需求
    1.使命型--合伙人--股权分红
    2.事业型--经理人--年度奖励
    3.发展型--优秀员工--提成工资
    4.保障型--普通员工--基本工资
5.工作目标
    1.今天重点完成哪项工作
    2.
激励员工的根本前提
1.如何让员工拥有明确的个人目标，没有目标的帮员工做决策
    

二部分：让员工知道团队的目标
1.目标提出后，下属不认可怎么办？
    1.让下属清楚
2.给下属一个清晰有效的目标
    1.例会：
    2.实战练习
    3.目标分享要有仪式感

三部分：通过团队目标实现个人目标
1.让组织的每个人都和团队的目标统一起来
2.

### 二、知识点 （重点，建议背诵）
### 调程案例总结
1.王兴如何挽留核心人才？沈鹏
    美团未来会怎么样，美团未来需要年轻人，反复说了这个观点。
    美团是第三产业本地生活服务的电子商务
    阿里是第二产业的电子商务
    沈鹏目标：跟兴哥学习，被团队需要，冲市场第一，做伟大公司
    人的目标有层次，人的目标会随着时间和变化而变化
2.连续三年留不住年度优秀员工
    1.不关注员工的真实想法
    2.单方面决定绩效奖金
    3.不尊重选人用人常识
3.我优衣库--柳井正
    1.要不厌其烦地一次一次向成员 传达，都能准备理解团队的共同目标
4.目标要拆成具体的，让员工明白，每个目标超出怎么样

    
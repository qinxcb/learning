

# 一 WebFlux编程基础
 1.1 Reactor
 是基于Reactive Streams规范的第四代响应库，用于在JVM上构建非阻塞的应用程序。
 1.2 Reactive Streams
 为无阻塞背压的异步流处理提供标准。
 1.3 Reactive Processing
 是一种范式（规范），它使开发人员能够构建非阻塞的、异步的应用程序，这些应用程序能够处理背压（流控制）
 1.4 Project Reactor 
 是一个完全无阻塞的基础，其中包括背压支持。它是Spring生态系统中的响应式堆栈的基础，并且在诸如Spring WebFlux，Spring Data和Spring Cloud Gateway等项目中都有它的身影。利用Project Reactor可以高效的响应式系统。刚才说Reactive Streams是规范，那么Project Reactor就是实现。
 1.5 WebFlux

 1.6 Flux和Mono
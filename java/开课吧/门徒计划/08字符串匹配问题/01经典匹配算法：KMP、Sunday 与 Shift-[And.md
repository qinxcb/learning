
### 暴力匹配
1.母串S ，就是字符比较多的串
2.模式串T:就是找母串的子串
暴力匹配算法，用模式串对齐毌串每一位。

暴力匹配作用：这个世界上存在一种虽然笨，但能正确的处理单模匹配算法。
不重不漏。 
第一位匹配了，不会再匹配，
算法重复就会影响效率。
查找过程中只有一个模式串。
`
int brute_force(const char *text, const char *pattern) {
    for (int i = 0; text[i]; ++i) {
        int flag = 1;
        for (int j = 0; pattern[j]; ++j) {
            if (text[i + j] == pattern[j]) continue;
            flag = 0;
            break;
        }
        if (flag) return i;
    }
    return -1;
}
`
### KMP
1.从模式串 -- 匹配  母串问题 
2.从模式串的前面--后模式串的后面对要对齐的模式算法问题
在处理的过程中，可以做预处理判断
<img src="01-01.png" alt="img" style="zoom:80%;" />
意义：
KMP 可以处理基于流数据的单模式匹配问题。
两台计算机的信息通讯，A每次给一个数据给B，B过滤敏感词，文章有多长不知道。
每传一个数据变化字符数据。
`
int GetNextJ(char ch, const char *pattern, int *next, int j) {
    while (j != -1 && ch - pattern[j + 1]) j = next[j];
    if (ch == pattern[j + 1]) j += 1;
    return j;
}

int kmp(const char *text, const char *pattern) {
    int n = strlen(pattern);
    int *next = (int *)malloc(sizeof(int) * n);
    GetNext(pattern, next);
    for (int i = 0, j = -1; text[i]; i++) {
        j = GetNextJ(text[i], pattern, next, j);
        if (pattern[j + 1] == 0) return i - j;
    }
    return -1;
}
`

### SUMDAY算法
1.黄金对齐点位，跳过匹配位置
2.一篇文章中找一个字符
3.实际场景中是 n/m 的数据，远优于KMP算法
4.要求对于文本是已知的
`
int sunday(const char *text, const char *pattern) {
    #define BASE 256
    int n = strlen(text), m, last_pos[BASE];
    for (int i = 0; i < BASE; i++) last_pos[i] = -1;
    for (m = 0; pattern[m]; ++m) last_pos[pattern[m]] = m;
    for (int i = 0; i + m <= n; i += (m - last_pos[text[i + m]])) {
        int flag = 1;
        for (int j = 0; pattern[j]; ++j) {
            if (text[i + j] == pattern[j]) continue;
            flag = 0;
            break;
        }
        if (flag) return i;
    }
    return -1;
}
`

### SHFT-and 算法
1.信息预处理：
 模式串，预处理一种特殊的信息格式
2.再用信息预处理
 信息+文本串，做匹配的过程
 模式串每一种字母的信息，转换成二进制
 字符串从左到右，从低到高

`
int GetNextP(char ch, int *code, int p) {
    return (p << 1 | 1) & code[ch];
}

int shift_and(const char *text, const char *pattern) {
    int code[256] = {0};
    int n = 0;
    for (n = 0; pattern[n]; ++n) code[pattern[n]] |= (1 << n);
    int p = 0;//代表当前位置，向前找能匹配模式串的前几位。
    for (int i = 0; text[i]; i++) {
        p = GetNextP(text[i], code, p);
        if (p & (1 << (n - 1))) return i - n + 1;
    }
    return -1;
}

`
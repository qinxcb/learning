### 一、基数排序
    1.基数排序的特性：稳定性,在排序过程，相应的数值相等不会改变位置
    2.基数排序的过程：
        1）统计每个基数存在的数量
        2）求区间和：从最小开始，得到该基数排序后的最后一个的位置
        3）从最一位置开始，当前位置的值 = （-- 该值基数值位置区间和）
    3.实现代码
```
    // 获取低位的基数
    int getLowRadix(int value);
    // 获取高位的基数
    int getHightRadix(int value);

    void radix_sort(int[] arr, int n) {
        int[] tmp = new int[n]; //存放排序后的值
        int maxRadix = maxRadix(arr); //获取最大的基数值
        int[] cnt = new int[maxRadix]; //
        for(int i = 0; i < n; i++) {
            cnt[getLowRadix(arr[i])] ++; //统计每个低位基数的数量
        }
        for(int i = 1; i < maxRadix; i++) {
            cnt[i] += cnt[i-1];  //求区间和
        }
        for(int i = n; i >= 0; i--) {
            tmp[-- cnt[getLowRadix(arr[i])]] = arr[i] ;// 设置位置
        }
        //重新初始化基数统计
        cnt = null;
        cnt = new int[n+1];
        for(int i = 0; i < n; i++) {
            cnt[getHightRadix(arr[i])] ++; //统计每个高位基数的数量
        }
        for(int i = 1; i < maxRadix; i++) {
            cnt[i] += cnt[i-1];  //求区间和
        }
        for(int i = n; i >= 0; i--) {
            tmp[-- cnt[getHightRadix(arr[i])]] = arr[i] ; // 设置位置
        }
        for(int i = 0; i < n; i++) {
            arr[i] = tmp[i]; //统计每个低位基数的数量
        }
        tmp = null;
        return ;
    }
```
### 二、拓朴排序
    1.特点：拓朴是一个队列，每次可入队的数据为度为0的结点，也就是它没有其他前置结点
    2.拓朴排序的

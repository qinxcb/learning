### 互联网的标准
    互联网的标准技术架构如下图所示，这张图基本上涵盖了互联网技术公司的大部分技术点，不同的公司只是在具体的技术实现上稍有差异，但不会跳出这个框架的范畴。
<img src="img/03-01-01.jpg" alt="img" style="zoom:80%;" />

### SQL
    SQL 即我们通常所说的关系数据
    数据库拆分满足了性能的要求，但带来了复杂度的问题：数据如何拆分、数据如何组合？这个复杂度的问题解决起来并不容易，如果每个业务都去实现一遍，重复造轮子将导致投入浪费、效率降低，业务开发想快都快不起来。MySQL Router、360 开源的数据库中间件 Atlas.
    SQL 集群，就会导致新的复杂度问题，具体表现在：
    . 数据库资源使用率不高，比较浪费。
    . 各 SQL 集群分开维护，投入的维护成本越来越高。
    淘宝的 UMP（Unified MySQL Platform）系统
### NoSQL
    NoSQL 发展到一定规模后，通常都会在 NoSQL 集群的基础之上再实现统一存储平台，统一存储平台主要实现这几个功能：
    . 资源动态按需动态分配：例如同一台 Memcache 服务器，可以根据内存利用率，分配给多个业务使用。
    . 资源自动化管理：例如新业务只需要申请多少 Memcache 缓存空间就可以了，无需关注具体是哪些 Memcache 服务器在为自己提供服务。
    . 故障自动化处理：例如某台 Memcache 服务器挂掉后，有另外一台备份 Memcache 服务器能立刻接管缓存请求，不会导致丢失很多缓存数据。
### 小文件存储
    SQL 和 NoSQL 不同的是，小文件存储不一定需要公司或者业务规模很大，基本上认为业务在起步阶段就可以考虑做小文件统一存储。得益于开源运动的发展和最近几年大数据的火爆，在开源方案的基础上封装一个小文件存储平台并不是太难的事情。例如，HBase、Hadoop、Hypertable、FastDFS 等都可以作为小文件存储的底层平台，只需要将这些开源方案再包装一下基本上就可以用了。
### 大文件存储
    . 一类是业务上的大数据，例如 Youtube 的视频、电影网站的电影；
    . 一类是海量的日志数据
    大文件，特别要提到 Google 和 Yahoo，Google 的 3 篇大数据论文（Bigtable/Map- Reduce/GFS）开启了一个大数据的时代，而 Yahoo 开源的 Hadoop 系列（HDFS、HBase 等），基本上垄断了开源界的大数据处理
<img src="img/03-02-01.jpg" alt="img" style="zoom:80%;" />

### 问题
    既然存储技术发展到最后都是存储平台，为何没有出现存储平台的开源方案，但云计算却都提供了存储平台方案？
1. 存储平台的开发成本高，由于存储平台是核心的平台，高可用，高性能是必须的，这就导致需要经验丰富的高级工程师来开发。而云平台作为服务提供商，有能力开发出来存储平台。
2. 需要使用存储平台的公司不多，而且一般是大型的公司，小公司的业务规模都不大，对于存储平台的需求基本不高，云平台面向的是所以用户，众口难调，必然提供基础服务
3. 云平台的存储平台是收费的，能为企业带来经济效益，而开源的存储平台，投入巨大，能使用的却很少，也就失去了意义

总结一下，云平台的存储平台，面向的是所有用户，包括大公司，有这方面的需求，而且是收费的，能够为平台带来收入；开源存储平台，服务的用户很少，投入巨大，所以没有

服务器容量规划设计
1. 初步估计，参考业界类似开源系统的性能，评估系统性能上限，例如做消息队列可以参考RocketMQ；
2. 性能测试，等系统出来后做压测，测试系统真实性能；
3. 逐步调优，绝大部分系统不会一开始就做到最优，逐步优化才是现实途径
## JVM解释
JVM是一种规范，
https://docs.oracle.com/javase/specs/index.html
是虚构出来的一台计算机，包含有
1, 字节指令集-汇编语言
2，内存管理：堆，栈，方法区


## 常见的JVM实现
1，Hotspot
   - oracle 官方，我们做实验用的JVM
   - Java -verison
2, Jrockit
   - BEA ,曾经号称世界上最快的JVM
   - 被Oracel收购，合并到Hotspot
3, J9--IBM
4, Microsoft VM 
5, Taobao VM
   - hotspot 深度定制版本
6, LiquidVM
   - 直接针对硬件
7, azul zing :很贵
   - 最新垃圾回收的业界标杆
   - ww.azul.com


JDK = Jre + develop kit

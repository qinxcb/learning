### 一.Lambda 表达式
    1、语法格式
        (parameters) -> expression
        或
        (parameters) ->{ statements; }
    2、示例：
    ```
        // 1. 不需要参数,返回值为 5  
        () -> 5  
        
        // 2. 接收一个参数(数字类型),返回其2倍的值  
        x -> 2 * x  
        
        // 3. 接受2个参数(数字),并返回他们的差值  
        (x, y) -> x – y  
        
        // 4. 接收2个int型整数,返回他们的和  
        (int x, int y) -> x + y  
        
        // 5. 接受一个 string 对象,并在控制台打印,不返回任何值(看起来像是返回void)  
        (String s) -> System.out.print(s)
    ```
### 二.方法引用
    1、构造器引用：它的语法是Class::new，或者更一般的Class< T >::new实例如下：
        final Car car = Car.create( Car::new );
        final List< Car > cars = Arrays.asList( car );
    2、静态方法引用：它的语法是Class::static_method，实例如下：
        cars.forEach( Car::collide );
    3、特定类的任意对象的方法引用：它的语法是Class::method实例如下：
        cars.forEach( Car::repair );
    4、特定对象的方法引用：它的语法是instance::method实例如下：
        final Car police = Car.create( Car::new );
        cars.forEach( police::follow );
### 三.函数式接口
    1	BiConsumer<T,U>
    代表了一个接受两个输入参数的操作，并且不返回任何结果
    2	BiFunction<T,U,R>
    代表了一个接受两个输入参数的方法，并且返回一个结果
    3	BinaryOperator<T>
    代表了一个作用于于两个同类型操作符的操作，并且返回了操作符同类型的结果
    35	Predicate<T>
    口是一个函数式接口，它接受一个输入参数 T，返回一个布尔值结果。
    36	Supplier<T>
    无参数，返回一个结果。
    43	UnaryOperator<T>
    接受一个参数为类型T,返回值类型也为T。
### 四、默认方法
    简单说，默认方法就是接口可以有实现方法，而且不需要实现类去实现其方法。
    我们只需在方法名前面加个 default 关键字即可实现默认方法。
    他们的目的是为了解决接口的修改与现有的实现不兼容的问题。
### 五、Stream
    Stream（流）是一个来自数据源的元素队列并支持聚合操作
    Java中的Stream并不会存储元素，而是按需计算。
    数据源 流的来源。 可以是集合，数组，I/O channel， 产生器generator 等。
    聚合操作 类似SQL语句一样的操作， 比如filter, map, reduce, find, match, sorted等。
    1.Collection操作不同， Stream操作还有两个基础的特征：
        a. Pipelining: 中间操作都会返回流对象本身。 这样多个操作可以串联成一个管道， 如同流式风格（fluent style）。 这样做可以对操作进行优化， 比如延迟执行(laziness)和短路( short-circuiting)。
        b. 内部迭代： 以前对集合遍历都是通过Iterator或者For-Each的方式, 显式的在集合外部进行迭代， 这叫做外部迭代。 Stream提供了内部迭代的方式， 通过访问者模式(Visitor)实现。
    2. 生成流
        a. stream() − 为集合创建串行流。
        a. parallelStream() − 为集合创建并行流。
    3. forEach
        Stream 提供了新的方法 'forEach' 来迭代流中的每个数据。以下代码片段使用 forEach 输出了10个随机数：
    4. map
        map 方法用于映射每个元素到对应的结果，以下代码片段使用 map 输出了元素对应的平方数：
        ```
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        // 获取对应的平方数
        List<Integer> squaresList = numbers.stream().map( i -> i*i).distinct().collect(Collectors.toList());
        ```
    5. filter
        filter 方法用于通过设置的条件过滤出元素。以下代码片段使用 filter 方法过滤出空字符串：
        ```
        List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        // 获取空字符串的数量
        long count = strings.stream().filter(string -> string.isEmpty()).count();

        ```
    6. limit
        limit 方法用于获取指定数量的流。 以下代码片段使用 limit 方法打印出 10 条数据：
        ```
        Random random = new Random();
        random.ints().limit(10).forEach(System.out::println);
        ```
    7. sorted
        sorted 方法用于对流进行排序。以下代码片段使用 sorted 方法对输出的 10 个随机数进行排序：
        ```
        Random random = new Random();
        random.ints().limit(10).sorted().forEach(System.out::println);
        ```
    8. 并行（parallel）程序
        parallelStream 是流并行处理程序的代替方法。以下实例我们使用 parallelStream 来输出空字符串的数量：
        ```
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        long count = strings.parallelStream().filter(string -> string.isEmpty()).count();
        ```
    9. Collectors
        Collectors 类实现了很多归约操作，例如将流转换成集合和聚合元素。Collectors 可用于返回列表或字符串：
        ```
        List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        String mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining(", "));
        ```
    10. 统计
        它们主要用于int、double、long等基本类型上，它们可以用来产生类似如下的统计结果。
        ```
        IntSummaryStatistics stats = numbers.stream().mapToInt((x) -> x).summaryStatistics();
        System.out.println("列表中最大的数 : " + stats.getMax());
        System.out.println("列表中最小的数 : " + stats.getMin());
        System.out.println("所有数之和 : " + stats.getSum());
        System.out.println("平均数 : " + stats.getAverage());
        ```
### 六、Optional 类
    static <T> Optional<T> empty()
    返回空的 Optional 实例。

    boolean equals(Object obj)
    判断其他对象是否等于 Optional。

    Optional<T> filter(Predicate<? super <T> predicate)
    如果值存在，并且这个值匹配给定的 predicate，返回一个Optional用以描述这个值，否则返回一个空的Optional。

    <U> Optional<U> flatMap(Function<? super T,Optional<U>> mapper)
    如果值存在，返回基于Optional包含的映射方法的值，否则返回一个空的Optional

    T get()
    如果在这个Optional中包含这个值，返回值，否则抛出异常：NoSuchElementException
    
    int hashCode()
    返回存在值的哈希码，如果值不存在 返回 0。

    void ifPresent(Consumer<? super T> consumer)
    如果值存在则使用该值调用 consumer , 否则不做任何事情。

    <U>Optional<U> map(Function<? super T,? extends U> mapper)
    如果有值，则对其执行调用映射函数得到返回值。如果返回值不为 null，则创建包含映射返回值的Optional作为map方法返回值，否则返回空Optional。

    T orElse(T other)
    如果存在该值，返回值， 否则返回 other。

    T orElseGet(Supplier<? extends T> other)
    如果存在该值，返回值， 否则触发 other，并返回 other 调用的结果。
### 七、 Nashorn JavaScript
    Nashorn 一个 javascript 引擎。
    Nashorn JavaScript Engine 在 Java 15 已经不可用了。
    ```
      ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
      ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");
        
      String name = "Runoob";
      Integer result = null;
      
      try {
         nashorn.eval("print('" + name + "')");
         result = (Integer) nashorn.eval("10 + 2");
         
      }catch(ScriptException e){
         System.out.println("执行脚本错误: "+ e.getMessage());
      }
    ```
### 八、 日期时间 API
    旧版的 Java 中，日期时间 API 存在诸多问题，其中有：
    非线程安全 − java.util.Date 是非线程安全的，所有的日期类都是可变的，这是Java日期类最大的问题之一。
    设计很差 − Java的日期/时间类的定义并不一致，在java.util和java.sql的包中都有日期类，此外用于格式化和解析的类在java.text包中定义。java.util.Date同时包含日期和时间，而java.sql.Date仅包含日期，将其纳入java.sql包并不合理。另外这两个类都有相同的名字，这本身就是一个非常糟糕的设计。
    时区处理麻烦 − 日期类并不提供国际化，没有时区支持，因此Java引入了java.util.Calendar和java.util.TimeZone类，但他们同样存在上述所有的问题。
    LocalDate/LocalTime 和 LocalDateTime 类可以在处理时区不是必须的情况。代码如下：

### 九、Base64：java.util.Base64;
    Base64工具类提供了一套静态方法获取下面三种BASE64编解码器：
    基本：输出被映射到一组字符A-Za-z0-9+/，编码不添加任何行标，输出的解码仅支持A-Za-z0-9+/。
    URL：输出映射到一组字符A-Za-z0-9+_，输出是URL和文件。
    MIME：输出隐射到MIME友好格式。输出每行不超过76字符，并且使用'\r'并跟随'\n'作为分割。编码输出最后没有行分割。











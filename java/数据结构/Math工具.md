### Arrays工具类
    1. Arrays.sort() 排序, 参数 Comparator比较器
    2. Arrays.binarySearch() 二分查找 
    3. Arrays.fill() 二分查找 
### Collections工具类
    1、排序操作
        1.void reverse(List list)：反转
        2.void shuffle(List list),随机排序
        3.void sort(List list),按自然排序的升序排序
        4.void sort(List list, Comparator c);定制排序，由Comparator控制排序逻辑
        5.void swap(List list, int i , int j),交换两个索引位置的元素
        6.void rotate(List list, int distance),旋转
    2、查找，替换操作
        1.int binarySearch(List list, Object key), 对List进行二分查找，返回索引，注意List必须是有序的
        2.int max(Collection coll),根据元素的自然顺序，返回最大的元素。 类比int min(Collection coll)
        3.int max(Collection coll, Comparator c)，根据定制排序，返回最大元素，排序规则由Comparatator类控制。类比int min(Collection coll, Comparator c)
        4.void fill(List list, Object obj),用元素obj填充list中所有元素
        5.int frequency(Collection c, Object o)，统计元素出现次数
        6.int indexOfSubList(List list, List target), 统计targe在list中第一次出现的索引，找不到则返回-1，类比int lastIndexOfSubList(List source, list target).
        7.boolean replaceAll(List list, Object oldVal, Object newVal), 用新元素替换旧元素。
    3. 同步控制
        SynchronizedList(), SynchronizedSet()等方法，来将集合包装成线程安全的集合。
        Collections.synchronizedList(new ArrayList());
        Collections.synchronizedSet(new HashSet());

### Math工具类
属性	描述
E	    返回算术常量 e，即自然对数的底数（约等于2.718）。
LN2	    返回 2 的自然对数（约等于0.693）。
LN10	返回 10 的自然对数（约等于2.302）。
LOG2E	返回以 2 为底的 e 的对数（约等于 1.414）。
LOG10E	返回以 10 为底的 e 的对数（约等于0.434）。
PI	    返回圆周率（约等于3.14159）。
SQRT1_2	返回返回 2 的平方根的倒数（约等于 0.707）。
SQRT2	返回 2 的平方根（约等于 1.414）。

方法	描述
abs(x)	返回数的绝对值。
acos(x)	返回数的反余弦值。
asin(x)	返回数的反正弦值。
atan(x)	以介于 -PI/2 与 PI/2 弧度之间的数值来返回 x 的反正切值。
atan2(y,x)	返回从 x 轴到点 (x,y) 的角度（介于 -PI/2 与 PI/2 弧度之间）。
ceil(x)	对数进行上舍入。
cos(x)	返回数的余弦。
exp(x)	返回 e 的指数。
floor(x)	对数进行下舍入。
log(x)	    返回数的自然对数（底为e）。
max(x,y)	返回 x 和 y 中的最高值。
min(x,y)	返回 x 和 y 中的最低值。
pow(x,y)	返回 x 的 y 次幂。
random()	返回 0 ~ 1 之间的随机数。
round(x)	把数四舍五入为最接近的整数。
sin(x)	返回数的正弦。
sqrt(x)	返回数的平方根。
tan(x)	返回角的正切。
toSource()	返回该对象的源代码。
valueOf()	返回 Math 对象的原始值。
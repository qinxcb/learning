### 数据集合的结构图

<img src="img/queue-01-1.png" alt="img" style="zoom:75%;" />   

### Queue：队列通常（但并非一定）以 FIFO（先进先出）; 
    1. JAVA实现类：LinkedList，Queue是FIFO的单端队列
    2. 方法
        boolean  add(E e) //将指定的元素插入此队列（如果立即可行且不会违反容量限制），在成功时返回 true，如果当前没有可用的空间，则抛出 IllegalStateException。
        E        element() //获取，但是不移除此队列的头。
        boolean  offer(E e) //将指定的元素插入此队列（如果立即可行且不会违反容量限制），当使用有容量限制的队列时，此方法通常要优于 add(E)，后者可能无法插入元素，而只是抛出一个异常。
        E        peek() //获取但不移除此队列的头；如果此队列为空，则返回 null。
        E        poll()  //获取并移除此队列的头，如果此队列为空，则返回 null。
        E        remove() //获取并移除此队列的头。
        E        removeFirst() //获取并移除此队列的头。
        E        removeLast() //获取并移除此队列尾。

        插入	add(e)	    offer(e)
        移除	remove()	poll()
        检查	element()	peek()

### Deque堆栈操作方法：双端结束的队列，双端队列，可以在首尾插入或删除元素
    双向队列是指该队列两端的元素既能入队(offer)也能出队(poll),如果将Deque限制为只能从一端入队和出队，则可实现栈的数据结构。
    对于栈而言，有入栈(push)和出栈(pop)，遵循先进后出原则。
    1. JAVA实现类： LinkedList,  ArrayDeque
        offerLast
        pollLast
        pollFirst
        peekFirst
    2. 特殊方法
        push(), 我们只要将要入栈的元素放到数组的末尾
        pop(),  出栈，并删除数据
        peek(), peek是指的返回栈顶端的元素，我们对栈本身不做任何的改动


### Stack 栈是Vector的一个子类，它实现了一个标准的后进先出的栈
    1. JAVA实现类：Stack，继承 Vector
    2. 特殊方法
        push(), 入栈
        pop(),  出栈并删除数据
        peek(), 取值不删除数据
### Queue 方法	等效 Deque 方法 
    add(e)	    addLast(e)
    offer(e)	offerLast(e)
    remove()	removeFirst()
    poll()	    pollFirst()
    element()	getFirst()
    peek()	    peekFirst()
### Stack 堆栈方法	等效 Deque 方法
    push(e)	    addFirst(e)
    pop()	    removeFirst()
    peek()	    peekFirst()

### 相应的使用：
    双端队列也可用作 LIFO（后进先出）堆栈。应优先使用此接口而不是遗留 Stack 类
    PriorityQueue 可以作为堆使用，而且可以根据传入的Comparator实现大小的调整，会是一个很好的选择。
    ArrayDeque 通常作为栈或队列使用，但是栈的效率不如LinkedList高。
    LinkedList 通常作为栈或队列使用，但是队列的效率不如ArrayQueue高。



### PriorityQueue：优先队列; 
    1. JAVA实现接口和类:AbstractQueue,Queue
    2. 构造函数：
        public PriorityQueue(Comparator<? super E> comparator) {
        this(DEFAULT_INITIAL_CAPACITY, comparator);
         }
         相应的对象必须有该处理。
    2. 方法
        boolean  add(E e) //将指定的元素插入此队列（如果立即可行且不会违反容量限制），在成功时返回 true，如果当前没有可用的空间，则抛出 IllegalStateException。
        E        element() //获取最大或最小的值，但是不移除此队列的头。
        E        peek()  //获取最大或最小的值，但是不移除此队列的头。
        
        boolean  offer(E e) //将指定的元素插入此队列（如果立即可行且不会违反容量限制），当使用有容量限制的队列时，此方法通常要优于 add(E)，后者可能无法插入元素，而只是抛出一个异常。
        
        E        poll()  //获取并移除此队列的头，如果此队列为空，则返回 null。
        E        remove() //获取并移除此队列的头。

        E        removeFirst() //获取并移除此队列的头。
        E        removeLast() //获取并移除此队列尾。

### 19[删除链表中的倒数第K个元素]https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/
    解题思路
        1.定义一个链表A顺序找到第k个元素的位置，这个用于找位置
        2.定义另一个链表B，A只要不断向下找，B只要找n-k个数是要删除结点这个数的前一位
        3.链表B的下一个元素 = 下一个元素的下一个元素
    ```
    class Solution {
        public ListNode removeNthFromEnd(ListNode head, int n) {
            ListNode ret = new ListNode(0, head), p = ret, q = head;
            //顺序找到第n个位置
            while (n-- > 0) {
                q = q.next;
            }
            //顺序剩余数量就是 顺序的位置，找到该位置的前一位
            while (q != null ) {
                q = q.next;
                p = p.next;
            }
            p.next = p.next.next;
            return ret.next;
        }
    }
    ```
    复杂度：
        1.时间 O(L), 数组的长度
        2.空间 O(1)
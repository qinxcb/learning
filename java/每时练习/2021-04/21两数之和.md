### [11. 盛水](https://leetcode-cn.com/problems/symmetric-tree/submissions/)
    解法：双向指针
        在一开始，双指针指向数组的左右边界，表示 数组中所有的位置都可以作为容器的边界，因为我们还没有进行过任何尝试。在这之后，我们每次将 对应的数字较小的那个指针 往 另一个指针 的方向移动一个位置，就表示我们认为 这个指针不可能再作为容器的边界了。
    复杂度：
        时间：O(n) 数组每个值最多遍历一次
        空间：O(1) 只有常数级的空间
    ```
         public int maxArea(int[] height) {
            int r = height.length -1;
            int i=0;
            int sum = 0;
            while(i<r){
                int tmp = Math.min(height[r],height[i]) * (r-i);
                sum = Math.max(tmp,sum);
                if(height[i] > height[r]){
                    r--;
                }else{
                    i++;
                }
            }
            return sum;
        }
    ```

### [1. 两数之和](https://leetcode-cn.com/problems/two-sum)
    解法一：暴力解
    复杂度：
        时间：O(n) 数组每个值最多遍历一次
        空间：O(1) 只有常数级的空间
    ```
        public int[] twoSum(int[] nums, int target) {
            int size = nums.length;
            for(int i=0;i<size;i++){
                for(int j=i+1;j<size;j++){
                    if(nums[i]+nums[j]==target){
                        return new int[]{i,j};
                    }
                }
            }
            return new int[0];
        }
    ```
    解法二：hash数处理法
    复杂度：
        时间：O(n) 数组每个值最多遍历一次
        空间：O(n) 
    ```
        public int[] twoSum(int[] nums, int target) {
            Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();
            for (int i = 0; i < nums.length; ++i) {
                if (hashtable.containsKey(target - nums[i])) {
                    return new int[]{hashtable.get(target - nums[i]), i};
                }
                hashtable.put(nums[i], i);
            }
            return new int[0];
        }

    ```
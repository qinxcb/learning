### 130.被围绕的区域(https://leetcode-cn.com/problems/surrounded-regions/)
#### 方案一：深度优先算法
    步骤：
    1. 找到边界的O，先设置一个中间值#
    2. 找到和边界相邻的结点O，也设置为#
    3. 再把所有的 O设置为X，#设置为O
```
class Solution {
    public void solve(char[][] board) {
        if (board == null || board.length == 0) return;
        int m = board.length;
        int n = board[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                //找临界点,并判断是为为'O'
                boolean isEdge = i == 0 || j == 0 || i == m - 1 || j == n - 1;
                if (isEdge && board[i][j] == 'O') {
                    dfs(board, i, j);
                }
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    board[i][j]  = 'X';
                } else if (board[i][j] == '#') {
                    board[i][j] = 'O';
                }
            }
        }
    }
    public void dfs(char[][] board, int i, int j) {
        if (i < 0 || j < 0 || i >= board.length || j >== board[0].length || 
        board[i][j] == 'X' || board[i][j] == '#') {
            return ;
        }
        board[i][j] = '#';
        //找到该位置的四个点
        dfs(board, i - 1, j);
        dfs(board, i + 1, j);
        dfs(board, i, j - 1);
        dfs(board, i, j + 1);
    }
}
```
    复杂度：
    1.时间:O(m * n) 矩阵的长和宽
    2.空间:O(m * n) 矩阵的长和宽，深度
    
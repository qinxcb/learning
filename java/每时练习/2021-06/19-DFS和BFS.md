### 993.二叉树的堂兄弟节点(https://leetcode-cn.com/problems/cousins-in-binary-tree/)
#### 方案一：深度优先算法
    步骤：
    1.找到x值的深度和父结点
    2.找到y值的深度和父结点
    3.深度一致，你父结点不一致返回真
```
class Solution {
    public boolean isCousins(TreeNode root, int x, int y) {
        int[] xv = dfs(root, null, 0, x);
        int[] yv = dfs(root, null, 0, y);
        return xv[0] == yv[0] && xv[1] != yv[1];
    }

    public int[] dfs(TreeNode root, TreeNode father, int d, int v) {
        if (root == null) return new int[]{-1, -1};
        if (root.val == v) return new int[]{d, father==null? 1:father.val};
        int[] l= dfs(root.left, root, d +1, v);
        if (l[0] != -1) {
          return l;
        }
        return dfs(root.right, root, d +1, v);
    }
}
```
    复杂度：
    1.时间:O(n) 最坏情况每个结点都要遍历
    2.空间:O(n) 最坏情况每层一个结点就是n层
    
#### 方案二：广度优先算法
    步骤：
    1.找到x值的深度和父结点
    2.找到y值的深度和父结点
    3.深度一致，你父结点不一致返回真
```
class Solution {
    public boolean isCousins(TreeNode root, int x, int y) {
        int[] xv = bfs(root, x);
        int[] yv = bfs(root, y);
        return xv[0] == yv[0] && xv[1] != yv[1];
    }

    public int[] bfs(TreeNode root, int target) {
        Queue<Object[]> queue = new LinkedList();
        queue.offer(new Object[]{root, null, 0});
        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size-- > 0) {
                Object[] tmp = queue.poll();
                TreeNode cur = (TreeNode)tmp[0], pa = (TreeNode)tmp[1];
                int d = (int)tmp[2];
                //找到相应的值直接返回
                if (cur.val == target) return new int[]{d, pa == null? 0:pa.val};
                if (cur.left !=null) queue.offer(new Object[]{cur.left, cur, d + 1});
                if (cur.right !=null) queue.offer(new Object[]{cur.right, cur, d + 1});
            }
        }
        return new int[]{-1, -1};
    }
}
```
    复杂度：
    1.时间:O(n) 最坏情况每个结点都要遍历
    2.空间:O(n) 最坏情况每层一个结点就是n层
### 494. 目标和(https://leetcode-cn.com/problems/target-sum/)
    解题方法：
        1. (每个使用目标值 - 相应的数) + (每个使用目标值 + 相应的数) 就是相应的结果
        2. 每个数有两种可能 要么 + 要么 -
```
class Solution {
    public int findTargetSumWays(int[] nums, int target) {
        int ret = dfs(nums, target, 0);
        return ret;
    }
    public int dfs(int[] nums, int target, int x) {
        if (x == nums.length) return target ==0 ? 1:0;
        return dfs(nums, target - nums[x], x + 1) + dfs(nums, target + nums[x], x + 1);
    }
}

```
    复杂度：
        时间:O(2^n)) 要遍历每一层，每个结点访问2(n-1)*n
        空间:O(1) 只需要用一个常量x

### 473. 火柴正方形(https://leetcode-cn.com/problems/matchsticks-to-square/)
    解题方法：
        1. 先判断总和是不是能除4除尽
        2. 计算每个边长的长度
        3. 通过DFS不断匹配每个边长,找出相应的数据
```
class Solution {
     public boolean makesquare(int[] matchsticks) {
        if (matchsticks == null || matchsticks.length < 4) return false;
        int sum = Arrays.stream(matchsticks).sum();
        if (sum % 4 != 0) return false;
        int sideLength = sum / 4;
        //正方形
        int[] square = new int[4];
        Arrays.fill(square, sideLength);
        Arrays.sort(matchsticks);
        return dfs(matchsticks.length - 1, square, matchsticks);
    }
    
    public int dfs(int idx, int[] square , int[] sticks) {
        // 遍历完成，证明已经符合要求
        if (idx == -1) return true;
        for (int i = 0; i < 4; i++) {
            //如果当前的值大于剩下数据，需要向后找
            if (sticks[idx] > square[i]) continue;
            if (sticks[idx] == square[i] || square[i] >= sticks[idx] + sticks[0]) {
                square[i] -= sticks[idx];
                //找到了就确定是否完成
                if (dfs(idx - 1, square, stick)) return true;
                //找不到，当前的火柴不行
                square[i] += sticks[idx];
            }
        }
        return false;
    }
}
```

402 移掉K位数字[https://leetcode-cn.com/problems/remove-k-digits/]
解题思路:
    1.
```
    class Solution {
        public String removeKdigits(String num, int k) {
            if (k >= num.length()) return "0";
            String ret ="";
            for (int i =0; i < num.length(); i ++) {
                while (k > 0 && ret.length() > 0 &&  
                    ret.substring(ret.length() - 1).compareTo(num.substring(i, i+1)) > 0) {
                    ret = ret.substring(0, ret.length() - 1);
                    k --;
                }
                ret += num.substring(i , i+1);
            }
            if (k > 0) {
                ret = ret.substring(0, ret.length() - k);
            }
            int ind = 0;
            while(ind < ret.length() && ret.charAt(ind) == '0') ++ind;
            ret = ret.substring(ind);
            if (ret.equals("")) return  "0";
            return ret;
        }
    }
```
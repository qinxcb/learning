# 理解auth2.0协议

作者：饿了爸
链接：https://www.jianshu.com/p/640fb2dae771
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

### 一 什么是OAuth协议
    1. 协议名称
    OAuth协议，是一种授权协议，不涉及具体的代码，只是表示一种约定的流程和规范。OAuth协议一般用于用户决定是否把自己在某个服务商上面的资源（比如：用户基本资料、照片、视频等）授权给第三方应用访问。此外，OAuth2.0协议是OAuth协议的升级版，现在已经逐渐成为单点登录（SSO）和用户授权的标准。
    2. 授权设计简单理解
    第一步，门禁系统的密码输入器下面，增加一个按钮，叫做"获取授权"。快递员需要首先按这个按钮，去申请授权。
    第二步，他按下按钮以后，屋主（也就是我）的手机就会跳出对话框：有人正在要求授权。系统还会显示该快递员的姓名、工号和所属的快递公司。
    我确认请求属实，就点击按钮，告诉门禁系统，我同意给予他进入小区的授权。
    第三步，门禁系统得到我的确认以后，向快递员显示一个进入小区的令牌（access token）。令牌就是类似密码的一串数字，只在短期内（比如七天）有效。
    第四步，快递员向门禁系统输入令牌，进入小区。
    3. 运行流程

<img src="img/auth-1.0.png" alt="img" style="zoom:75%;" />

    （A）用户打开客户端以后，客户端要求用户给予授权。
    （B）用户同意给予客户端授权。
    （C）客户端使用上一步获得的授权，向认证服务器申请令牌。
    （D）认证服务器对客户端进行认证以后，确认无误，同意发放令牌。
    （E）客户端使用令牌，向资源服务器申请获取资源。
    （F）资源服务器确认令牌无误，同意向客户端开放资源。

### 二 OAuth2.0授权方式
    1.授权码模式
<img src="img/auth-2.1.png" alt="img" style="zoom:75%;" />

    2.简化模式
<img src="img/auth-2.2.png" alt="img" style="zoom:75%;" />

    3.用户名密码模式
<img src="img/auth-2.3.png" alt="img" style="zoom:75%;" />

    4.客户端凭证
<img src="img/auth-2.4.png" alt="img" style="zoom:75%;" />

    适用于服务器见通信场景，机密客户代表它自己或者一个用户
    只有后端渠道，使用客户凭证获取一个access token
    因为客户凭证可以使用对称或者非对称加密，该方式支持共享spring密码或者证书

### 三 OAuth2.0的JAVA实现
https://blog.csdn.net/wangxuelei036/article/details/109491215
    1，数据结构：
        CREATE TABLE `oauth_client_details` (
        //用于唯一标识每一个客户端(client); 在注册时必须填写(也可由服务端自动生成).
        `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL, 
        //客户端所能访问的资源id集合,多个资源时用逗号(,)分隔,如: "unity-resource,mobile-resource".
        `resource_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
        //用于指定客户端(client)的访问密匙; 在注册时必须填写(也可由服务端自动生成).
        //对于不同的grant_type,该字段都是必须的. 在实际应用中的另一个名称叫appSecret,与client_secret是同一个概念.
        `client_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
        //指定客户端申请的权限范围,可选值包括read,write,trust;若有多个权限范围用逗号(,)分隔,如: "read,write".
        //@EnableGlobalMethodSecurity(prePostEnabled = true)启用方法级权限控制
        //然后在方法上注解标识@PreAuthorize("#oauth2.hasScope('read')")
        `scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
        //指定客户端支持的grant_type,可选值包括authorization_code,password,refresh_token,implicit,client_credentials
        //若支持多个grant_type用逗号(,)分隔,如: "authorization_code,password".
        `authorized_grant_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
        //客户端的重定向URI,可为空, 当grant_type为authorization_code或implicit时, 在Oauth的流程中会使用并检查与注册时
        // 填写的redirect_uri是否一致. 下面分别说明:
        `web_server_redirect_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
        `authorities` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
        `access_token_validity` int(11) NOT NULL,
        `refresh_token_validity` int(11) DEFAULT NULL,
        //	这是一个预留的字段,在Oauth的流程中没有实际的使用,可选,但若设置值,必须是JSON格式的数据,如:
        //{"country":"CN","country_code":"086"}
        `additional_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
        //设置用户是否自动Approval操作, 默认值为 'false', 可选值包括 'true','false', 'read','write'.
        //该字段只适用于grant_type="authorization_code"的情况,当用户登录成功后,若该值为'true'或支持的scope值,
        // 则会跳过用户Approve的页面, 直接授权.
        `autoapprove` tinyint(4) DEFAULT NULL,
        `origin_secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
        PRIMARY KEY (`client_id`) USING BTREE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='客户端配置表';

package kaike;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Class0315_969,
 * 煎饼算法.
 * 题目意思：里面的数字和index不一致
 *
 * @author 一辉
 * @version 1.0
 * @className Class0315_969 <br/>
 * @date 2021-03-16 8:40
 */
public class Class0315_969 {

  public static void main(String[] args) {
    int[] arr = new int[]{9, 8, 4, 1, 3, 2, 6, 5, 7};
    Class0315_969 instant = new Class0315_969();
    // System.out.println(instant.pancakeSort(arr));

    System.out.println(instant.pancakeSort2(arr));
    //https://xue.kaikeba.com/api/mentu/videos/9887565442322.mp4
    // 97562454
    // 98875654423221
    // 9887565442322
  }

  public List<Integer> pancakeSort(int[] arr) {
    List<Integer> result = new ArrayList<>();
    for (int i = arr.length; i > 0; i--) {
      int index = 0;
      while (arr[index] != i) {
        index++;
      }
      if (index == i - 1) {
        continue;
      } else if (index == 0) {
        reverse(arr, i);
        result.add(i);
      } else {
        reverse(arr, index + 1);
        result.add(index + 1);
        reverse(arr, i);
        result.add(i);
      }
    }
    return result;
  }

  public void reverse(int[] arr, int index) {
    int start = 0, end = index - 1;
    while (start < end) {
      int temp = arr[end];
      arr[end] = arr[start];
      arr[start] = temp;
      start++;
      end--;
    }
  }

  /**
   * 第二种做法
   * @param arr
   * @return
   */
  public List<Integer> pancakeSort2(int[] arr) {
    //存放相应的位置
    int[] ind = new int[arr.length + 1];
    for (int i = 0; i < arr.length; i++) ind[arr[i]] = i;
    List<Integer> resutl = new LinkedList<>();
    for (int i = arr.length; i >= 1; i--) {
      // if( ind[i] == i-1 ) continue;
      if (ind[i] + 1 != 1) {
        resutl.add(ind[i] + 1);
        reverse2(arr, ind[i] + 1, ind);
      }
      if (i != 1) {
        resutl.add(i);
        reverse2(arr, i, ind);
      }
    }
    return resutl;
  }
  /**
   * 第二种做法
   * @param arr
   * @return
   */
  public void reverse2(int[] arr, int index, int[] ind) {
    for (int i = 0, j = index - 1; i < j; i++, j--) {
      int tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
      ind[arr[i]] = i;
      ind[arr[j]] = j;
    }
    return;
  }

}

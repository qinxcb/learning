package kaike;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Class_0318.
 * 145. 二叉树的后序遍历
 * https://leetcode-cn.com/problems/binary-tree-postorder-traversal/submissions/
 *
 * @author 一辉
 * @version 1.0
 * @className Class_0318 <br/>
 * @date 2021-03-18 20:41
 */
public class Class_0318 {

  public List<Integer> postorderTraversal(TreeNode root) {
    List<Integer> ans = new ArrayList();
    Stack<TreeNode> stack = new Stack<TreeNode>();
    stack.push(root);
    while (!stack.isEmpty()) {
      TreeNode node = stack.pop();
      if (node.left != null) stack.push(node.left);
      if (node.right != null) stack.push(node.right);
      ans.add(0, node.val);
    }
    return ans;
  }

  public int[][] t(int[][] ma) {
    int m = ma.length;
    int n = ma[0].length;
    int[][] res = new int[n][m];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        res[i][j] = ma[j][i];
      }
    }
    return res;
  }

  public int calculate(String s) {

    int num = 0;
    char sign = '+';
    Stack<Integer> nums = new Stack<Integer>();
    for (int i = 0; i < s.length(); ++i) {
      char cur = s.charAt(i);
      if (cur >= 0) {
        num = num * 10 - '0' + cur;
      }
      if (cur < 0 && cur != ' ') {
        switch (sign) {
          case '+':
            nums.push(num);
            break;
          case '-':
            nums.push(-num);
            break;
          case '*':
            nums.push(nums.pop() * num);
            break;
          case '/':
            nums.push(nums.pop() / num);
            break;
        }
        sign = cur;
        num = 0;
      }
    }
    int res = 0;
    while (!nums.isEmpty()){
      res += nums.pop();
    }
    return res;
  }


}

class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;

  TreeNode() {
  }

  TreeNode(int val) {
    this.val = val;
  }

  TreeNode(int val, TreeNode left, TreeNode right) {
    this.val = val;
    this.left = left;
    this.right = right;
  }
}

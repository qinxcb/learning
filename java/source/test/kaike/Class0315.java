package kaike;

/**
 * 给你一个链表的头节点 head 和一个特定值 x ，请你对链表进行分隔，使得所有 小于 x 的节点都出现在 大于或等于 x 的节点之前。
 * <p>
 * 你应当 保留 两个分区中每个节点的初始相对位置。
 * <p>
 *   #86. 分隔链表
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/partition-list
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * <p>
 * Class0315.
 *
 * @author 一辉
 * @version 1.0
 * @className Class0315 <br/>
 * @date 2021-03-15 18:57
 */
public class Class0315 {

  public  static void main(String[] args){
    ListNode head = new ListNode(1);
    head.next = new ListNode(4);
    head.next.next = new ListNode(3);
    head.next.next.next = new ListNode(2);
    head.next.next.next.next = new ListNode(5);
    head.next.next.next.next.next = new ListNode(2);
    ListNode newT = partition(head,3);
    while (newT!=null) {
      System.out.print("," + newT.val);
      newT = newT.next;
    }
    System.out.println();
  }
  public static ListNode partition(ListNode head, int x) {
    ListNode small = new ListNode(0);
    ListNode smallHair = small;
    ListNode larger = new ListNode(0);
    ListNode largerHair = larger;
    while (head!=null){
      if(head.val<x){
        small.next = head;
        small = small.next;
      }else{
        larger.next = head;
        larger = larger.next;
      }
      head = head.next;
    }
    larger.next = null;
    small.next = largerHair.next;
    return smallHair.next;
  }
}

class ListNode {
  int val;
  ListNode next;

  ListNode() {
  }

  ListNode(int val) {
    this.val = val;
  }

  ListNode(int val, ListNode next) {
    this.val = val;
    this.next = next;
  }

  @Override
  public String toString() {
    return "ListNode{" +
        "val=" + val +
        ", next=" + next +
        '}';
  }
}
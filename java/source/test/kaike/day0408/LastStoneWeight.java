package kaike.day0408;

import java.util.PriorityQueue;

/**
 * LastStoneWeight.
 *
 * @author 一辉
 * @version 1.0
 * @className LastStoneWeight <br/>
 * @date 2021-04-09 8:45
 */
public class LastStoneWeight {

  public static  void main(String[] args){

    lastStoneWeight(new int[]{2,7,4,1,8,1});
    lastStoneWeight2(new int[]{2,7,4,1,8,1});
  }
  public static  int lastStoneWeight(int[] stones) {
    LastStoneWeight stoneWeight = new LastStoneWeight(stones.length);
    for(int i:stones){
      stoneWeight.push(i);
    }
    stoneWeight.output(stoneWeight.cnt);
    return  1;
  }

  public static  int lastStoneWeight2(int[] stones) {
    PriorityQueue<Integer> pq = new PriorityQueue<Integer>((a, b) -> b - a);
    for (int stone : stones) {
      pq.offer(stone);
    }
    System.out.println(pq.toString());

    while (pq.size() > 1) {
      int a = pq.poll();
      int b = pq.poll();
      if (a > b) {
        pq.offer(a - b);
      }
    }
    return pq.isEmpty() ? 0 : pq.poll();
  }

  public LastStoneWeight(int size){
    this.data = new int[size +5];
  }

  int data[] ;
  int cnt = 0;
  int top() { return data[0]; }
  int size() { return cnt; }

  public  void shift_up(int ind) {
    while (ind > 0 && data[(ind - 1) / 2] < data[ind]) {
      swap(data[(ind - 1) / 2], data[ind]);
      ind = (ind - 1) / 2;
    }
    return ;
  }

  void output(int n) {
    System.out.println("heap : ");
    for (int i = 0; i < n; i++) {
      System.out.print(data[i] +" " );
    }
    System.out.println("\n");
    return ;
  }

  void shift_down(int ind) {
    int n = cnt - 1;
    while (ind * 2 + 1 <= n) {
      int temp = ind;
      if (data[temp] < data[ind * 2 + 1]) temp = ind * 2 + 1;
      if (ind * 2 + 2 <= n && data[temp] < data[ind * 2 + 2]) temp = ind * 2 + 2;
      if (temp == ind) break;
      swap(data[temp], data[ind]);
      ind = temp;
    }
    return ;
  }

  void push(int x) {
    data[cnt++] = x;
    shift_up(cnt-1);
    return ;
  }

  void pop() {
    if (size() == 0) return ;
    swap(data[0], data[cnt - 1]);
    cnt -= 1;
    shift_down(0);
    return ;
  }
  void swap(int i,int b){
    int tmp = data[i];
    data[i] = data[b];
    data[b] = tmp;
  }

}

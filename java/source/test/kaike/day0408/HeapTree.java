package kaike.day0408;


/**
 * heap.
 *
 * @author 一辉
 * @version 1.0
 * @className heap <br/>
 * @date 2021-04-08 20:40
 */
public class HeapTree implements Tree {
  /**
   * 树根节点
   */
  private Object element;
  /**
   * 父节点、长子及最大的弟弟
   */
  private HeapTree parent, firstChild, nextSibling;

  @Override
  public Object getElem() {
    return null;
  }

  @Override
  public Object setElem(Object obj) {
    return null;
  }

  @Override
  public HeapTree getParent() {
    return null;
  }

  @Override
  public HeapTree getFirstChild() {
    return null;
  }

  @Override
  public HeapTree getNextSibling() {
    return null;
  }

  @Override
  public int getSize() {
    return 0;
  }

  @Override
  public int getHeight() {
    return 0;
  }

  @Override
  public int getDepth() {
    return 0;
  }
}

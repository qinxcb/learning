package kaike;

/**
 * #138. 分隔链表
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/partition-list
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * <p>
 * Class0315.
 *
 * @author 一辉
 * @version 1.0
 * @className Class0315 <br/>
 * @date 2021-03-15 18:57
 */
public class Class0315_138 {

  public static void main(String[] args) {

  }

  public static Node copyRandomList(Node head) {
    if (head == null) return head;
    // 空间复杂度O(1)，将克隆结点放在原结点后面
    Node node = head;
    // 1->2->3  ==>  1->1'->2->2'->3->3'
    while (node != null) {
      Node clone = new Node(node.val);
      clone.next = node.next;
      node.next = clone; //克隆的放下一个数
      node = clone.next;
    }
    // 处理random指针指向复制的结点
    node = head;
    while (node.next != null) {
      node.next.random = node.random == null ? null : node.random.next;
      node = node.next.next;
    }
    // 还原原始链表，即分离原链表和克隆链表
    node = head;
    Node cloneHead = head.next;
    while (node != null) {
      node.next = node.next.next;
      cloneHead.next = (node.next.next ==null ? null:node.next.next);
      node = node.next;
    }
    return cloneHead;
  }

  public Node copyRandomList1(Node head) {
    if (head == null) return null;
    Node pointer = head;
    while
    (pointer != null) {
      Node newNode = new Node(pointer.val);
      newNode.next = pointer.next;
      pointer.next = newNode;
      pointer = newNode.next;
    }
    pointer = head;
    while (pointer != null) {
      pointer.next.random = (pointer.random != null) ? pointer.random.next : null;
      pointer = pointer.next.next;
    }
    Node pointerOldList = head;
    Node pointerNewList = head.next;
    Node newHead = head.next;
    while (pointerOldList != null) {
      pointerOldList.next = pointerOldList.next.next;
      pointerNewList.next = (pointerNewList.next != null) ? pointerNewList.next.next : null;
      pointerOldList = pointerOldList.next;
      pointerNewList = pointerNewList.next;
    }
    return newHead;
  }
}

class Node {
  int val;
  Node next;
  Node random;

  public Node(int val) {
    this.val = val;
    this.next = null;
    this.random = null;
  }
}
package day02;

import java.util.Arrays;

/**
 * StackByArrayAutoCapacity.
 *
 * @author 一辉
 * @version 1.0
 * @className StackByArrayAutoCapacity <br/>
 * @date 2021-04-08 9:08
 */
public class StackByArrayAutoCapacity {
  String[] arrays;
  int autoCapacity;
  int count;
  int index;

  public StackByArrayAutoCapacity(int n) {
    arrays = new String[n];
    autoCapacity = n;
    count = n;
    index = 0;
  }

  public boolean push(String item) {
    if (index == count) {
      String[] temp = arrays;
      arrays = Arrays.copyOf(temp,count + autoCapacity);
      count = count + autoCapacity;
    }
    arrays[index] = item;
    index++;
    return true;
  }

  public String pop() {
    if (index == 0) {
      return null;
    }
    String temp = arrays[index - 1];
    index--;
    return temp;
  }

  public String peek() {
    if (index == 0) {
      return null;
    }
    String temp = arrays[index - 1];
    return temp;
  }

  public boolean empty() {
    return index == 0;
  }

  public String toString(){
    StringBuffer sb = new StringBuffer();
    for(int i=0;i<index;i++){
      sb.append(arrays[i]+"\n");
    }
    return sb.toString();
  }
}

package day02;

import java.util.PriorityQueue;

/**
 * TestArray.
 *
 * @author 一辉
 * @version 1.0
 * @className TestArray <br/>
 * @date 2021-04-08 9:13
 */
public class TestArray {

  public static void main(String[] args){
//    StackByArrayAutoCapacity auto = new StackByArrayAutoCapacity(4);
//    for(int i=0;i<50;i++){
//      auto.push("test:" +i);
//    }
//    System.out.println(auto.toString());
    System.out.println(firstMissingPositive(new  int[]{1,2,0}));
  }

  public static int firstMissingPositive(int[] nums) {
    int n = nums.length;
    for (int i = 0; i < n; i++) {
      if (nums[i] <= 0) {
        nums[i] = (n + 1);
      }
    }
    for (int i = 0; i < n; i++) {
      int tmp = Math.abs(nums[i]);
      if (tmp <= n) {
        nums[tmp - 1] = -Math.abs(nums[tmp - 1]);
      }
    }
    for (int i = 0; i < n; i++) {
      if (nums[i] > 0) {
        return i + 1;
      }
    }
    return n + 1;
  }
}

package day02;

/**
 * 数组实现顺序栈; 后进先出.
 *
 * @author xcb
 * @date 2021-04-07 19:52
 */
public class LinkStatck<T> {

  private Item dummy = new Item("dummy");
  //作为栈中最后一个节点
  private Item head;

  LinkStatck() {
    head = dummy;
  }

  boolean pop() {
    if (dummy.next == null) {
      return false;
    } else {
      head = head.before;
      head.next = null;
      return true;
    }
  }

  boolean push(String val) {
    Item item = new Item(val);
    item.next = null;
    item.before = head;
    head.next = item;
    head = head.next;
    return true;
  }
}

class Item {
  String val;
  //使用双向链表来处理pop
  Item next;
  Item before;

  Item(String val) {
    this.val = val;
  }
}

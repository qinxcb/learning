package day01;

import java.util.PriorityQueue;

/**
 *给你一个链表数组，每个链表都已经按升序排列。
 *
 * 请你将所有链表合并到一个升序链表中，返回合并后的链表。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：lists = [[1,4,5],[1,3,4],[2,6]]
 * 输出：[1,1,2,3,4,4,5,6]
 * 解释：链表数组如下：
 * [
 *   1->4->5,
 *   1->3->4,
 *   2->6
 * ]
 * 将它们合并到一个有序链表中得到。
 * 1->1->2->3->4->4->5->6
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/merge-k-sorted-lists
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @className Class01LinkhasCycle <br/>
 * @date 2021-03-31 9:13
 */
public class Class01LinkmergeKLists2 {
  public static void main(String[] args) {
    ListNode[] listNodes = new ListNode[2];
    ListNode result = mergeKLists(listNodes);
    System.out.println(result);

  }
  static PriorityQueue<Status>  queue = new PriorityQueue<Status>();

  public static ListNode mergeKLists(ListNode[] lists) {
    for (ListNode node: lists) {
      if (node != null) {
        queue.offer(new Status(node.val, node));
      }
    }
    ListNode head = new ListNode(0);
    ListNode tail = head;
    while (!queue.isEmpty()) {
      Status f = queue.poll();
      tail.next = f.ptr;
      tail = tail.next;
      if (f.ptr.next != null) {
        queue.offer(new Status(f.ptr.next.val, f.ptr.next));
      }
    }
    return head.next;
  }

  static class Status implements Comparable<Status> {
    int val;
    ListNode ptr;

    Status(int val, ListNode ptr) {
      this.val = val;
      this.ptr = ptr;
    }

    public int compareTo(Status status2) {
      return this.val - status2.val;
    }
  }

}


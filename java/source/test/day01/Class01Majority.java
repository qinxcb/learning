package day01;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 三数之和
 * <p>
 * 给你一个包含n个整数的数组nums，判断nums中是否存在三个元素a，b，c，使得a+b+c=0？请你找出所有满足条件且不重复的三元组
 * 注意：答案中不可以包含重复的三元组
 * 给定数组nums=[-1,0,1,2,-1,-4]
 * 满足要求的三元组集合为：[ [-1,0,1],[-1,-1,2] ].
 *
 * 思路：
 * 1，先排序
 * 2，遍历
 *     1. 如果当前的值大于0，就不需要了，不然会重复
 *     2. 如果当前的值
 *
 * @author 一辉
 * @version 1.0
 * @className class01.class01 <br/>
 * @date 2021-03-02 15:14
 */
public class Class01Majority {

  public static void main(String[] args) {
    int[] nums = {6,5,5};
    int result = majorityElement(nums);
    System.out.println(result);
    result = majorityElement2(nums);
    System.out.println(result);
  }

  public static int majorityElement(int[] nums) {
    Map<Integer, Integer> numSize = new HashMap<Integer, Integer>();
    int max = 0;
    int res = 0;
    int big = nums.length/2 +1;
    for (int i : nums) {
      numSize.put(i, numSize.getOrDefault(i,0)+1);
      int size = numSize.get(i);
      if (size > max) {
        max = size;
        res = i;
        if(size>=big){
          break;
        }
      }
    }
    return res;
  }
  // 我们先将 nums 数组排序，然后返回上文所说的下标对应的元素。下面的图中解释了为什么这种策略是有效的。在下图中，第一个例子是 nn 为奇数的情况，第二个例子是 nn 为偶数的情况。
  //

  public static int majorityElement2(int[] nums) {
    Arrays.sort(nums);
    return nums[nums.length/2];
  }

  // Boyer-Moore 投票算法
  // 如果我们把众数记为 +1，把其他数记为 −1，将它们全部加起来，显然和大于 0，从结果本身我们可以看出众数比其他数多。
  public int majorityElement3(int[] nums) {
    int count = 0;
    Integer candidate = null;
    for (int num : nums) {
      if (count == 0) { //=0先标记为该数
        candidate = num;
      }
      count += (num == candidate) ? 1 : -1;
    }

    return candidate;
  }
}

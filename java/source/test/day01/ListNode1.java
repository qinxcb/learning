package day01;

/**
 * ListNode1.
 *
 * @author 一辉
 * @version 1.0
 * @className ListNode1 <br/>
 * @date 2021-04-07 19:18
 */
class ListNode1 {
  int val;
  ListNode1 next;

  ListNode1(int val) {
    this.val = val;
  }

}

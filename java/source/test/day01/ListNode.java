package day01;

/**
 * ListNode.
 *
 * @author 一辉
 * @version 1.0
 * @className ListNode <br/>
 * @date 2021-04-07 19:18
 */
class ListNode {
  int val;
  ListNode next;

  ListNode(int x) {
    val = x;
    next = null;
  }

  ListNode(int val, ListNode next) {
    this.val = val;
    this.next = next;
  }
}

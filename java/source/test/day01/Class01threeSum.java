package day01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 三数之和
 * <p>
 * 给你一个包含n个整数的数组nums，判断nums中是否存在三个元素a，b，c，使得a+b+c=0？请你找出所有满足条件且不重复的三元组
 * 注意：答案中不可以包含重复的三元组
 * 给定数组nums=[-1,0,1,2,-1,-4]
 * 满足要求的三元组集合为：[ [-1,0,1],[-1,-1,2] ].
 *
 * 思路：
 * 1，先排序
 * 2，遍历
 *     1. 如果当前的值大于0，就不需要了，不然会重复
 *     2. 如果当前的值
 *
 * @author 一辉
 * @version 1.0
 * @className class01.class01 <br/>
 * @date 2021-03-02 15:14
 */
public class Class01threeSum {

  public static void main(String[] args) {
    int[] nums = {-1, 0, 1, 2, -1, -4};
    List<List<Integer>> result = threeSum(nums);
    for (List<Integer> obj : result) {
      System.out.println(Arrays.asList(obj));
    }
  }

  public static List<List<Integer>> threeSum(int[] nums) {
    if (nums.length < 3) {
      return new ArrayList<>();
    }
    Arrays.sort(nums);
    List<List<Integer>> result = new LinkedList<>();
    for (int i = 0; i < nums.length; i++) {
      if (nums[i] > 0) {
        break;
      }
      if (i > 0 && nums[i] == nums[i - 1]) continue; // 去重
      int l = i + 1;
      int r = nums.length - 1;
      while (l < r) {
        int sum = nums[i] + nums[l] + nums[r];
        if (sum == 0) {
          result.add(Arrays.asList(nums[i], nums[l], nums[r]));
          while (l < r && nums[l] == nums[l + 1]) { //去重
            l = l + 1;
          }
          while (l < r && nums[r] == nums[r - 1]) { //去重
            r = r - 1;
          }
          l++;
          r--;
        } else if (sum < 0) {
          l++;
        } else {
          r--;
        }
      }
    }
    return result;
  }
}

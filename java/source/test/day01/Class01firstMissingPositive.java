package day01;

/**
 * https://leetcode-cn.com/problems/first-missing-positive/
 给你一个未排序的整数数组 nums ，请你找出其中没有出现的最小的正整数。
 进阶：你可以实现时间复杂度为 O(n) 并且只使用常数级别额外空间的解决方案吗？
 示例 1：
 输入：nums = [1,2,0]
 输出：3
 示例 2：
 输入：nums = [3,4,-1,1]
 输出：2
 *
 * @author 一辉
 * @version 1.0
 * @className class01.class01 <br/>
 * @date 2021-03-02 15:14
 */
public class Class01firstMissingPositive {

  public static void main(String[] args) {
    int[] nums = {1,2,0};
    int result = firstMissingPositive2(nums);
    System.out.println(result);

  }

  public static int firstMissingPositive(int[] nums) {
    Integer[] tmp = new Integer[nums.length + 1];
    int size = nums.length;
    for (int i : nums) {
      if (i > 0 && i < size+1) {
        tmp[i] = i;
      }
    }
    int i = 1;
    while (i <= size) {
      if (tmp[i] == null) {
        return i;
      }
      i++;
    }
    return i;
  }

  /**
   * 时间复杂度为O(n)，空间复杂度为1
   * @param nums
   * @return
   */
  public static int firstMissingPositive2(int[] nums) {
    int size = nums.length;
    for (int i=0;i<size;i++) {
      if (nums[i] <= 0) {
        nums[i] = size + 1;
      }
    }
    for (int i = 0; i < size; ++i) {
      int num = Math.abs(nums[i]);
      if (num <= size) {
        //对应的位置数标识为负数
        nums[num - 1] = -Math.abs(nums[num - 1]);
      }
    }
    for (int i = 0; i < size; ++i) {
      if (nums[i] > 0) {
        return i + 1;
      }
    }
    return size + 1;
  }
}

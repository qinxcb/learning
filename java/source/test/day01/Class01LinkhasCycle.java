package day01;

import java.util.HashSet;
import java.util.Set;

/**
 *给定一个链表，判断链表中是否有环。
 *
 * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。 为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。
 * 如果 pos 是 -1，则在该链表中没有环。注意：pos 不作为参数进行传递，仅仅是为了标识链表的实际情况。
 *
 * 如果链表中存在环，则返回 true 。 否则，返回 false 。
 *https://leetcode-cn.com/problems/linked-list-cycle/solution/huan-xing-lian-biao-by-leetcode-solution/
 *  
 *
 * 进阶：
 *
 * 你能用 O(1)（即，常量）内存解决此问题吗？
 *
 * 作者：LeetCode-Solution
 * 链接：https://leetcode-cn.com/problems/linked-list-cycle/solution/huan-xing-lian-biao-by-leetcode-solution/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 * @className Class01LinkhasCycle <br/>
 * @date 2021-03-31 9:13
 */
public class Class01LinkhasCycle {
  public static void main(String[] args) {
    int[] nums = {1,2,0};
    ListNode1 head = new ListNode1(1);
    boolean result = hasCycle(head);
    System.out.println(result);

  }
  // 时间O(n),空间O(n)
  public static boolean hasCycle(ListNode1 head) {
    Set<ListNode1> seen = new HashSet<ListNode1>();
    while (head != null) {
      if (!seen.add(head)) {
        return true;
      }
      head = head.next;
    }
    return false;
  }
  // 我们可以根据上述思路来解决本题。具体地，我们定义两个指针，一快一满。慢指针每次只移动一步，而快指针每次移动两步。
  // 初始时，慢指针在位置 head，而快指针在位置 head.next。这样一来，如果在移动的过程中，快指针反过来追上慢指针，就说明该链表为环形链表。否则快指针将到达链表尾部，该链表不为环形链表。
  //

  // 时间O(n),空间O(1)
  public static boolean hasCycle2(ListNode1 head) {
    if (head == null || head.next == null) {
      return false;
    }
    ListNode1 slow = head;
    ListNode1 fast = head.next;
    while (slow != fast) {
      if (slow == null || fast.next ==null) {
        return  false;
      }
      slow = slow.next;
      fast = fast.next.next;
    }
    return true;
  }
}


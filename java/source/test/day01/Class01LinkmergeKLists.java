package day01;

/**
 *给你一个链表数组，每个链表都已经按升序排列。
 *
 * 请你将所有链表合并到一个升序链表中，返回合并后的链表。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：lists = [[1,4,5],[1,3,4],[2,6]]
 * 输出：[1,1,2,3,4,4,5,6]
 * 解释：链表数组如下：
 * [
 *   1->4->5,
 *   1->3->4,
 *   2->6
 * ]
 * 将它们合并到一个有序链表中得到。
 * 1->1->2->3->4->4->5->6
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/merge-k-sorted-lists
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @className Class01LinkhasCycle <br/>
 * @date 2021-03-31 9:13
 */
public class Class01LinkmergeKLists {
  public static void main(String[] args) {
    ListNode[] listNodes = new ListNode[2];
    ListNode result = mergeKLists(listNodes);
    System.out.println(result);

  }
  // 时间O(n),空间O(n)
  public static ListNode mergeKLists(ListNode[] lists) {
    if(lists.length ==0){
      return new ListNode(0,null);
    }
    if(lists.length==1){
      return lists[0];
    }
    ListNode ans = null;
    for (int i = 0; i < lists.length; ++i) {
      ans = mergeTwoLists(ans, lists[i]);
    }
    return ans;
  }
  // 我们可以根据上述思路来解决本题。具体地，我们定义两个指针，一快一满。慢指针每次只移动一步，而快指针每次移动两步。
  // 初始时，慢指针在位置 head，而快指针在位置 head.next。这样一来，如果在移动的过程中，快指针反过来追上慢指针，就说明该链表为环形链表。否则快指针将到达链表尾部，该链表不为环形链表。
  //

  public static ListNode mergeTwoLists(ListNode a, ListNode b) {
    if (a == null || b == null) {
      return a != null ? a : b;
    }
    ListNode head = new ListNode(0);
    ListNode tail = head, aPtr = a, bPtr = b;
    while (aPtr != null && bPtr != null) {
      if (aPtr.val < bPtr.val) {
        tail.next = aPtr;
        aPtr = aPtr.next;
      } else {
        tail.next = bPtr;
        bPtr = bPtr.next;
      }
      tail = tail.next;
    }
    tail.next = (aPtr != null ? aPtr : bPtr);
    return head.next;
  }
}


package day0429;

public class Leecode1319 {

    public int makeConnected(int n, int[][] connections) {
        if (connections.length < n - 1) return -1;
        UnionSet u = new UnionSet(n);
        for (int[] c : connections) {
            int a = c[0];
            int b = c[1];
            u.union(a, b);
        }
        int re = 0;
        for (int i = 0; i < n; i++) {
            if (u.find(i) == i) re++;
        }
        return re - 1;
    }
}

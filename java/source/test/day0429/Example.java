package day0429;

/**
 * @ClassName: Example
 * @Description:
 * @Author: xcb
 * @Date: 2021-05-15 12:37
 * @Version: V1.0
 */
public class Example {
  String str = new String("good");
  char[] ch = {'a', 'b', 'c'};

  public static void main(String args[]) {

    Example ex = new Example();
    ex.change(ex.str, ex.ch);

    System.out.print(ex.str + " and ");
    System.out.print(ex.ch);
  }

  public void change(String str, char ch[]) {

    str = "test ok";
    ch[0]='g';
  }
}

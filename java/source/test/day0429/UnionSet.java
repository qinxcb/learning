package day0429;

public class UnionSet {
    int[] boss;
    int value;

    public UnionSet(int n) {
        value = n + 1;
        boss = new int[n];
        for (int i = 0; i < n; i++) {
            boss[i] = i;
        }
    }

    public int find(int b) {
        return boss[b] = (boss[b] == b ? b : find(boss[b]));
    }

    public void union(int a, int b) {
        boss[find(a)] = find(b);
    }
}

package day0429;

public class Leecode990 {

    public boolean equationsPossible(String[] equations) {
        UnionSet u = new UnionSet(26);
        for (String str : equations) {
            if (str.charAt(1) == '!') continue;
            int a = str.charAt(0) - 'a';
            int b = str.charAt(3) - 'a';
            u.union(a, b);
        }

        for (String str : equations) {
            if (str.charAt(1) == '=') continue;
            int a = str.charAt(0) - 'a';
            int b = str.charAt(2) - 'a';
            if (u.find(a) == u.find(b)) return false;
        }
        return true;
    }
}

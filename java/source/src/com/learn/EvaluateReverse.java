package com.learn;

import java.util.Arrays;

public class EvaluateReverse {

    public static void  main(String[] args){
        String[] tokens = new String[]{"1","2","+"};
        System.out.println(evalRPN(tokens));
    }
    public static int evalRPN(String[] tokens) {
        Integer[] value = new Integer[tokens.length/2 +1];
        int idx =-1;
        for (String v:tokens) {
            switch (v) {
                case "+":
                    idx --;
                    value[idx] = value[idx] + value[idx+1];
                    break;
                case "-":
                    idx --;
                    value[idx] = value[idx] - value[idx+1];
                    break;
                case "*":
                    idx --;
                    value[idx] = value[idx] * value[idx+1];
                    break;
                case "/":
                    idx --;
                    value[idx] = value[idx] / value[idx+1];
                    break;
                default:
                    idx ++;
                    value[idx] = Integer.parseInt(v);
                    break;
            }
        }
        return value[idx];
    }
}

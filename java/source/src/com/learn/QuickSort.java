package com.learn;

public class QuickSort {
  public static void main(String[] args) {
		int[] arr = {5, 3, 6, 8, 1, 7, 9, 4, 2, 10};
//		int[] arr = {5, 3};
    //sort(arr, 0, arr.length-1);
    QuickSort sort = new QuickSort();
    sort.quick_sort_v3(arr, 0, arr.length - 1);
    print(arr);

  }

  public static void sort(int[] arr, int leftBound, int rightBound) {
    if (leftBound >= rightBound) return;
    int mid = partition(arr, leftBound, rightBound);
    sort(arr, leftBound, mid - 1);
    sort(arr, mid + 1, rightBound);
  }

  static int partition(int[] arr, int leftBound, int rightBound) {
    int pivot = arr[rightBound];
    int left = leftBound;
    int right = rightBound - 1;

    while (left <= right) {
      while (left <= right && arr[left] <= pivot) left++;
      while (left <= right && arr[right] > pivot) right--;
      if (left < right) swap(arr, left, right);
    }
    swap(arr, left, rightBound);

    return left;
  }

  static void swap(int[] arr, int i, int j) {
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }

  static void print(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i] + " ");
    }
  }

  int threshold = 16;

  int median(int a, int b, int c) {
    if (a > b) {
      int temp = b;
      b = a;
      a = temp;
    }
    if (a > c) {
      int temp = c;
      c = a;
      a = temp;
    }
    if (b > c) {
      return c;
    }
    return b;
  }

	public void __quick_sort_v2(int[] arr, int l, int r) {
		while (r > l ) {
			int x = l, y = r;
			//获取相应的中间值数据
			int base = arr[l];
			System.out.println( "x =" + x + " y " + y +" base = "+base);
			while (x < y) {
				while (x < y && arr[y] >= base) { y--; }
				if (x < y) {
					swap(arr, x, y);
					x++;
				}
				while (x < y && arr[x] <= base) { x++; }
				if (x < y) { arr[y--] = arr[x]; }
			};
			arr[x] = base;
			__quick_sort_v2(arr, x +1, r);
			r = x - 1;
		}
	}

	public void __quick_sort_v22(int[] arr, int l, int r) {
		while (r > l ) {
			int x = l, y = r;
			//获取相应的中间值数据
			int base = median(arr[l], arr[(l + r) / 2], arr[r]);
			while (x <= y) {
				while (arr[x] < base) { x++; }
				while (arr[y] > base) { y--; }
				if (x <= y) {
					swap(arr, x, y); x++ ; y--;
				}
			};
			__quick_sort_v22(arr, x, r);
			r = y;
		}
	}

  void __quick_sort_v3(int[] arr, int l, int r) {
    while (r - l > threshold) {
      int x = l, y = r;
      //获取相应的中间值数据
      int base = median(arr[l], arr[(l + r) / 2], arr[r]);
      do {
        while (arr[x] < base) {
          x++;
        }
        while (arr[y] > base) {
          y--;
        }
        if (x <= y) {
          swap(arr, x, y);
          x++;
          y--;
        }
      } while (x <= y);
      __quick_sort_v3(arr, x, r);
      r = y;
    }
  }

  //插入排序 无监督
  void final_insert_sort(int[] arr, int l, int r) {
    int ind = l;
    for (int i = l + 1; i <= r; i++) {
      if (arr[i] < arr[ind]) ind = i;
    }
    // 替换数据
    while (ind > l) {
      swap(arr, ind, ind - 1);
      ind--;
    }
    for (int i = l + 2; i <= r; i++) {
      int j = i;
      while (arr[j] < arr[j - 1]) {
        swap(arr, j, j - 1);
        j--;
      }
    }
  }

  void quick_sort_v3(int[] arr, int l, int r) {
    __quick_sort_v3(arr, l, r);
    final_insert_sort(arr, l, r);
  }
}

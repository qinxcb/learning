package com.learn.thread.testnum;

import java.util.concurrent.TimeUnit;

/**
 * TestUtil.
 *
 * @author 一辉
 * @version 1.0
 * @className TestUtil <br/>
 * @date 2021-03-17 14:12
 */
public class TestUtil {
  public static void doWorkByTime(long millisecond) {
    long startTime = System.currentTimeMillis();

    while (true) {

      long endTime = System.currentTimeMillis();
      long costTime = endTime - startTime;
      if (costTime >= millisecond) {
        return;
      }

    }

  }

  public static void doBlockOperation(long millisecond) throws InterruptedException {
    TimeUnit.MILLISECONDS.sleep(millisecond);
  }
}

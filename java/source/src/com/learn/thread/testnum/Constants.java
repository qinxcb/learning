package com.learn.thread.testnum;

/**
 * Constants.
 *
 * @author 一辉
 * @version 1.0
 * @className Constants <br/>
 * @date 2021-03-17 14:12
 */
public class Constants {
  /**任务量*/
  public static final int SIZE = 50000;
  /**线程数*/
  public static final int THREAD_SIZE = 8;

  /**阻塞系数*/
  public static final long BLOCK_TIME_MILLISECOND = 5;
  /**工作时长*/
  public static final int WORK_TIME_MILLISECOND = 5;
}

/**
  Number of Task :5000
  Number of Thread :1
  Cost Time:53599

 Number of Task :5000
 Number of Thread :5
 Cost Time:10980

 Number of Task :5000
 Number of Thread :10
 Cost Time:5534

 Number of Task :5000
 Number of Thread :20
 Cost Time:3494

 Number of Task :5000
 Number of Thread :50
 Cost Time:3403

 Number of Task :5000
 Number of Thread :100
 Cost Time:3507

 Number of Task :5000
 Number of Thread :1000
 Cost Time:3626

 Number of Task :5000
 Number of Thread :2000
 Cost Time:3744
 **/
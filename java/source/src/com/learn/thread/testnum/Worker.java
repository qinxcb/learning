package com.learn.thread.testnum;

import java.util.concurrent.CountDownLatch;

/**
 * Worker.
 *
 * @author 一辉
 * @version 1.0
 * @className Worker <br/>
 * @date 2021-03-17 14:12
 */
public class Worker implements Runnable {


  private CountDownLatch countDownLatch;

  public Worker(CountDownLatch countDownLatch) {


    this.countDownLatch = countDownLatch;
  }

  public void run() {


    try {
      while (countDownLatch.getCount() > 0) {

        //do work
        TestUtil.doWorkByTime(Constants.WORK_TIME_MILLISECOND);

        //do block operation
        TestUtil.doBlockOperation(Constants.BLOCK_TIME_MILLISECOND);

        countDownLatch.countDown();
      }
    } catch (InterruptedException e) {
    }

  }

}
